from flask import Blueprint, jsonify, request, current_app
from blogwise.models import Article

bp = Blueprint('home', __name__)


@bp.get('/api/v1/articles')
def all_articles():
    articles = Article.get_all()
    return jsonify([article.to_json() for article in articles]), 200


@bp.get('/api/v1/articles/<int:article_id>')
def get_article(article_id: int):
    article = Article.get_by_id(article_id)
    return jsonify(article.to_json()), 200


@bp.post('/api/v1/articles')
def create_article():
    data = request.json
    article = Article.create(**data)
    return jsonify(article.to_json()), 201


@bp.route('/api/v1/articles/<int:article_id>', methods=['POST', 'PATCH'])
def update_article(article_id: int):
    updated_article = Article.update(article_id, request.json.copy())
    return jsonify(updated_article.to_json()), 200


@bp.route('/api/v1/articles/<int:article_id>', methods=['DELETE'])
def delete_article(article_id: int):
    Article.delete(article_id)
    empty = Article.get_by_id(article_id)  # example 
    return jsonify(empty), 204
